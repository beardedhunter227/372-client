

public class AddIssueRequest extends IssueRequest{
	public Issue issue;
	public int projectId;
	public AddIssueRequest() { }
	public AddIssueRequest( Issue issue, int projectId ) {
		this.issue = issue;
		this.projectId = projectId;
	}

}
