
public class AddIssueResponse extends IssueResponse {
	int newId;
	  
	  public AddIssueResponse(int newId) {
	    this.newId = newId;
	  }

	  public int getNewId() {
	    return newId;
	  }

	  public void setNewId(int newId) {
	    this.newId = newId;
	  }
	  
	
}
