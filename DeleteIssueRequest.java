
public class DeleteIssueRequest extends IssueRequest{
	public Issue issue;
	public int projectId;
	public DeleteIssueRequest() { }
	public DeleteIssueRequest( Issue issue, int projectId  ) {
		this.issue = issue;
		this.projectId = projectId;
	}

}
