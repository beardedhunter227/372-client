
public class DeleteIssueResponse extends IssueResponse {
	  int newId;
	  
	  public DeleteIssueResponse(int newId) {
	    this.newId = newId;
	  }

	  public int getNewId() {
	    return newId;
	  }

	  public void setNewId(int newId) {
	    this.newId = newId;
	  }
}
