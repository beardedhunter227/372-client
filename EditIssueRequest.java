public class EditIssueRequest extends IssueRequest{
	public Issue issue;
	public int projectId;
	public EditIssueRequest() { }
	public EditIssueRequest( Issue issue, int projectId ) {
		this.issue = issue;
		this.projectId = projectId;
	}

}
