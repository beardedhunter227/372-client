
public class EditIssueResponse extends IssueResponse {
	  int newId;
	  
	  public EditIssueResponse(int newId) {
	    this.newId = newId;
	  }

	  public int getNewId() {
	    return newId;
	  }

	  public void setNewId(int newId) {
	    this.newId = newId;
	  }
}
