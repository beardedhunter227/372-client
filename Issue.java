import java.io.Serializable;


public class Issue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String title;
	private String desc;
	private Status status;
	
	public String toString(){
		return this.title + "(" + this.status + "): " + this.desc;
	}
	
	public Issue(int id, String title, String desc, Status status){
		this.id = id;
		this.title = title;
		this.desc = desc;
		this.status = status;
	}
	
//	@Override 
//	public boolean equals(Issue other){
//		return (id == other.id);
//	}
	
	@Override 
	public boolean equals(Object object){
		return (id == ((Issue)object).id);
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getDesc() {
		return desc;
	}
	public Status getStatus() {
		return status;
	}
	
	
}
