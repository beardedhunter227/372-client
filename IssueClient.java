
import com.google.gson.Gson;

//import src.com.github.kevinsawicki.http.HttpRequest;
//import issueTrackerClient.src.issueTrackerClient.*;
//import issueTrackerClient.src.*;


public class IssueClient {

      public static void main(String[] args) {
        Issue issue = new Issue(0, "title", "desc", Status.NEW);
       
        String jsonResponse = sendRequest(new AddIssueRequest(issue,0 ));
           
        AddIssueResponse response = new Gson().fromJson(jsonResponse, AddIssueResponse.class);
        System.out.println("New issue ID: " + response.getNewId());
       
        jsonResponse = sendRequest(new ListIssueRequest());

        System.out.println("list:");
        ListIssueResponse listResponse = new Gson().fromJson(jsonResponse, ListIssueResponse.class);
        System.out.println("Issue:" + listResponse.issues);

        jsonResponse = sendRequest(new DeleteIssueRequest(issue,0));
        
        System.out.println("delete:");
        DeleteIssueResponse deleteResponse = new Gson().fromJson(jsonResponse, DeleteIssueResponse.class);
        System.out.println("Deleted Issue:" + deleteResponse.getNewId());

        Issue newIssue = new Issue(1, "newTitle", "NewDesc", Status.INPROGRESS);
        jsonResponse = sendRequest(new EditIssueRequest(newIssue,0));
        
        System.out.println("edit:");
        EditIssueResponse editResponse = new Gson().fromJson(jsonResponse, EditIssueResponse.class);
        System.out.println("Edited Issue:" + editResponse.getNewId());
        
        jsonResponse = sendRequest(new ListIssueRequest());
        
        System.out.println("list:");
        listResponse = new Gson().fromJson(jsonResponse, ListIssueResponse.class);
        System.out.println("Issue:" + listResponse.issues);
        
      }
     
      static String sendRequest(IssueRequest req) {
        String jsonRequest = new Gson().toJson(req);
        String url = "http://localhost:8080/" + req.getClass().getSimpleName();
        System.out.println("Sending to " + url + " - " + jsonRequest);
       
        HttpRequest httpReq = HttpRequest.post(url).send(jsonRequest);
        int status = httpReq.code();
        String jsonResponse = httpReq.body();
       
        System.out.println("Received code " + status + " json " + jsonResponse);
        return jsonResponse;
           
      }
}
