import java.util.ArrayList;


public class IssueDatabase {
  
  private ArrayList<Issue> issues = new ArrayList<Issue>();
  
  public int add(Issue issue) {
    int newId = 0;
    for (Issue c : issues) {
      newId = Math.max(newId, c.getId());
    }
    
    ++newId;
    issue.setId(newId);
    issues.add(issue);
    return newId;
  }
  
  public int edit(Issue issue) {
	  for (int i = 0; i < issues.size() ; i++){
		  if (issues.get(i).getId() == issue.getId()){
			  issues.remove(i);
			  issues.add(issue);
			  break;
		  }
	  }
	  return issue.getId();
  }
  
  public int delete(Issue issue) {
	  for (int i = 0; i < issues.size() ; i++){
		  if (issues.get(i).getId() == issue.getId()){
			  issues.remove(i);
			  break;
		  }
	  }
	  return issue.getId();
  }

  public ArrayList<Issue> getAll() {
    return issues;
  }

  private static IssueDatabase instance = new IssueDatabase();
  
  public static IssueDatabase instance() {
    return instance;
  }
}