import java.util.ArrayList;


public class ListIssueResponse extends IssueResponse {
	public ArrayList<Issue> issues;
	
	public ListIssueResponse(ArrayList<Issue> issues) {
		this.issues = issues;
	}
}
