import com.google.gson.Gson;

//import contactclient.src.com.github.kevinsawicki.http.HttpRequest;
//import issueTrackerClient.src.issueTrackerClient.*;
//import issueTrackerClient.src.*;

public class ProjectClient {


          public static void main(String[] args) {
//        	  Project project;
            String jsonResponse = sendRequest(new ListProjectRequest());
               
            ListProjectResponse response = new Gson().fromJson(jsonResponse, ListProjectResponse.class);
            System.out.println("Projects " + response.projects);
           
//            jsonResponse = sendRequest(new ListProjectRequest());
//           
//            ListProjectResponse listResponse = new Gson().fromJson(jsonResponse, ListProjectResponse.class);
//            System.out.println("Issue:" + listResponse.projects);
//           
          }
         
          static String sendRequest(ProjectRequest req) {
            String jsonRequest = new Gson().toJson(req);
            String url = "http://localhost:8080/" + req.getClass().getSimpleName();
            System.out.println("Sending to " + url + " - " + jsonRequest);
           
            HttpRequest httpReq = HttpRequest.post(url).send(jsonRequest);
            int status = httpReq.code();
            String jsonResponse = httpReq.body();
           
            System.out.println("Received code " + status + " json " + jsonResponse);
            return jsonResponse;
               
          }
    }