import java.util.ArrayList;


public class ProjectDatabase {
  
  private ArrayList<Project> projects = new ArrayList<Project>();
  
//  public int add(Project project) {
//    int newId = 0;
//    for (Project c : projects) {
//      newId = Math.max(newId, c.getId());
//    }
//    
//    ++newId;
//    project.setId(newId);
//    projects.add(project);
//    return newId;
//  }

  public ArrayList<Project> getAll() {
    return projects;
  }
  
  public void clearAll(){
	  projects = new ArrayList<Project>();
  }
  
  public Project getProject(int a){
	  return projects.get(a);
  }

  private static ProjectDatabase instance = new ProjectDatabase();
  
  public static ProjectDatabase instance() {
    return instance;
  }
}