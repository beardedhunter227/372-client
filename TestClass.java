import static org.junit.Assert.*;

import java.io.IOException;
import java.net.BindException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;


public class TestClass {
	static Issue issue = new Issue(0,"First Issue","First!!!",Status.NEW);
	static issueServer server;

	@BeforeClass
	public static void setUp() throws Exception {
		issueServer.startUp();
	    server = new issueServer(8080);
	    try {
	      server.start();
	    } catch (BindException ioe) {
	      System.err.println("Couldn't start server: port is already in use (is another instance running?)");
	      System.exit(-1);
	    } catch (IOException ioe) {
	      System.err.println("Couldn't start server:\n" + ioe);
	      System.exit(-1);
	    }

	    System.out.println("Server started, Hit Enter to stop.\n");
	    
	    testPrint();
//		Issue issue = new Issue(0,"First Issue","First!!!",Status.NEW);

	}
	
	@Before
	public void space(){
		System.out.println();
	}

//	@Test
	public static void testPrint() {
		System.out.println("Projects:");
		System.out.println(ProjectDatabase.instance().getAll());
	}

	static String sendRequest(IssueRequest req) {
		String jsonRequest = new Gson().toJson(req);
		String url = "http://localhost:8080/" + req.getClass().getSimpleName();
		System.out.println("Sending to " + url + " - " + jsonRequest);
		
		HttpRequest httpReq = HttpRequest.post(url).send(jsonRequest);
		int status = httpReq.code();
		String jsonResponse = httpReq.body();
		
		System.out.println("Received code " + status + " json " + jsonResponse);
		return jsonResponse;
	}
	
	static String sendRequest(ProjectRequest req){
		return sendRequest(req);
	}
	
	@Test
	public void testAddIssueRequest(){
		System.out.println("Add issue \"First issues\":");
//		Issue issue = new Issue(0,"First Issue","First!!!",Status.NEW);
		String jsonResponse = sendRequest(new AddIssueRequest(issue,0));
		AddIssueResponse response = new Gson().fromJson(jsonResponse, AddIssueResponse.class);
		System.out.println(ProjectDatabase.instance().getProject(0).getIssues());
		assertTrue(response.isSuccess);
	}
	
	@Test
	public void testEditIssueRequest(){
		System.out.println("Edit issue \"First Issue\":");
		int newId = ProjectDatabase.instance().getProject(0).getIssues().get(0).getId();
		
		Issue newIssue = new Issue(newId,"newTitle","newDesc",Status.INPROGRESS);
		String jsonResponse = sendRequest(new EditIssueRequest(newIssue,0));
		EditIssueResponse response = new Gson().fromJson(jsonResponse, EditIssueResponse.class);
		System.out.println(ProjectDatabase.instance().getProject(0).getIssues());
		assertTrue(response.isSuccess);
		assertTrue(ProjectDatabase.instance().getProject(0).getIssues().get(0).equals(newIssue));
		
	}
	
	@Test
	public void testDeleteIssueRequest(){
		System.out.println("Delete issue \"First Issues\":");
		issue = ProjectDatabase.instance().getProject(0).getIssues().get(0);
		String jsonResponse = sendRequest(new DeleteIssueRequest(issue,0));
		DeleteIssueResponse response = new Gson().fromJson(jsonResponse, DeleteIssueResponse.class);
		System.out.println(ProjectDatabase.instance().getProject(0).getIssues());
		assertTrue(response.isSuccess);
	}
	
	@Test
	public void testPersistance() throws Exception{
		System.out.println("Persistance Test:");
		System.out.println("Write an issue:");
		ProjectDatabase.instance().getAll().get(0).addIssue(issue);
		System.out.println(ProjectDatabase.instance().getProject(0).getIssues());

		System.out.println("Stopping Server");
		server.stop();
		issueServer.shutDown();
		System.out.println("Starting Server");
		setUp();
		System.out.println(ProjectDatabase.instance().getProject(0).getIssues());
		
		
		
	}
	
}
