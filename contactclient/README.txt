README
======

1. Import the following directories into Eclipse (File > Import > Existing Projects Into Workspace)
   - contactclient
   - contactlib
   - contactserver

2. Start the contact server:
   Right-click contactserver/src/cpsc3720/contactserver/ContactServer.java and choose Run As > Java Application

3. Run the contact client:
   Right-click contactclient/src/cpsc3720/contactclient/ContactClient.java and choose Run As > Java Application
   This will attempt to add a new Contact to the (initially empty) in-memory Contact database on the server
   
Now, try the following:

4. Modify the contact client:
   Edit ContactClient.java and change the details of the contact to be added.
   Run the ContactClient again. This time, there should be two contacts displayed.
   
5. Modify the contact client and server to support Delete capability:
   - In the contactlib project, review the request and response message objects.
   - Add a new DeleteRequest class and corresponding DeleteResponse. Think about what data should be
     included in these classes.
   - Modify the ContactServer to delete the requested contact when it receives a DeleteRequest message object
   - Modify the ContactClient to attempt to delete a contact instead of adding one
