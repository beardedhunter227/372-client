package cpsc3720.contactclient;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import cpsc3720.contactserver.model.Contact;
import cpsc3720.contactserver.request.AddContactRequest;
import cpsc3720.contactserver.request.ContactRequest;
import cpsc3720.contactserver.request.ListContactRequest;
import cpsc3720.contactserver.response.AddContactResponse;
import cpsc3720.contactserver.response.ListContactResponse;

public class ContactClient {
  public static void main(String[] args) {
    Contact contact = new Contact(0, "Bev", "bev@somewhere.com");;
    
    String jsonResponse = sendRequest(new AddContactRequest(contact));
        
    AddContactResponse response = new Gson().fromJson(jsonResponse, AddContactResponse.class);
    System.out.println("New contact ID: " + response.getNewId());
    
    jsonResponse = sendRequest(new ListContactRequest());
    
    ListContactResponse listResponse = new Gson().fromJson(jsonResponse, ListContactResponse.class);
    System.out.println("Contacts:" + listResponse.contacts);
    
  }
  
  static String sendRequest(ContactRequest req) {
    String jsonRequest = new Gson().toJson(req);
    String url = "http://localhost:8080/" + req.getClass().getSimpleName();
    System.out.println("Sending to " + url + " - " + jsonRequest);
    
    HttpRequest httpReq = HttpRequest.post(url).send(jsonRequest);
    int status = httpReq.code();
    String jsonResponse = httpReq.body();
    
    System.out.println("Received code " + status + " json " + jsonResponse);
    return jsonResponse;
        
  }
}
