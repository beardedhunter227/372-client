package cpsc3720.contactserver.request;

import cpsc3720.contactserver.model.Contact;

public class AddContactRequest extends ContactRequest {
  public Contact contact;

  public AddContactRequest() { }
  
  public AddContactRequest(Contact contact) {
    this.contact = contact;
  }

}
