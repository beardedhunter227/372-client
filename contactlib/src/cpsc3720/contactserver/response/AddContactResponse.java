package cpsc3720.contactserver.response;

public class AddContactResponse extends ContactResponse {
  int newId;
  
  public AddContactResponse(int newId) {
    this.newId = newId;
  }

  public int getNewId() {
    return newId;
  }

  public void setNewId(int newId) {
    this.newId = newId;
  }
  
  
}
