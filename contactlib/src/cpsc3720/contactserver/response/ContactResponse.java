package cpsc3720.contactserver.response;

public class ContactResponse {

  protected boolean isSuccess;

  protected String errorMsg;
  
  public ContactResponse() {
    isSuccess = true;
  }
  
  public ContactResponse(String errorMsg) {
    isSuccess = false;
    this.errorMsg = errorMsg;
  }
  
  public boolean isSuccess() {
    return isSuccess;
  }
  
  public void setSuccess(boolean isSuccess) {
    this.isSuccess = isSuccess;
  }
  
  public String getErrorMsg() {
    return errorMsg;
  }
  
  public void setErrorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
  }
  
  
}
