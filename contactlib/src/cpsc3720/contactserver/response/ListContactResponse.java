package cpsc3720.contactserver.response;

import java.util.ArrayList;

import cpsc3720.contactserver.model.Contact;

public class ListContactResponse extends ContactResponse {
  
  public ArrayList<Contact> contacts;
  
  public ListContactResponse(ArrayList<Contact> contacts) {
    this.contacts = contacts;
  }

}
