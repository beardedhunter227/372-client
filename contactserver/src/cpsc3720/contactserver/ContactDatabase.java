package cpsc3720.contactserver;

import java.util.ArrayList;

import cpsc3720.contactserver.model.Contact;

public class ContactDatabase {
  
  private ArrayList<Contact> contacts = new ArrayList<Contact>();
  
  public int add(Contact contact) {
    int newId = 0;
    for (Contact c : contacts) {
      newId = Math.max(newId, c.getId());
    }
    
    ++newId;
    contact.setId(newId);
    contacts.add(contact);
    return newId;
  }

  public ArrayList<Contact> getAll() {
    return contacts;
  }

  private static ContactDatabase instance = new ContactDatabase();
  
  public static ContactDatabase instance() {
    return instance;
  }
}
