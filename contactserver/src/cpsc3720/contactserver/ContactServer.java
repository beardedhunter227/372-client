package cpsc3720.contactserver;

import java.io.IOException;
import java.io.InputStream;
import java.net.BindException;

import com.google.gson.Gson;

import cpsc3720.contactserver.request.AddContactRequest;
import cpsc3720.contactserver.request.ListContactRequest;
import cpsc3720.contactserver.response.AddContactResponse;
import cpsc3720.contactserver.response.ContactResponse;
import cpsc3720.contactserver.response.ListContactResponse;
import fi.iki.elonen.NanoHTTPD;

public class ContactServer extends NanoHTTPD {

  public ContactServer(int port) {
    super(port);
  }

  @Override
  public synchronized Response serve(IHTTPSession session) {

    String path = session.getUri();
    String body = getBody(session);
    Object result;
    System.out.println("Incoming request: path = " + path + "; body: " + body);
    
    if (path.equals("/AddContactRequest")) {
      AddContactRequest request = new Gson().fromJson(body, AddContactRequest.class);
      result = processAddContactRequest(request);
    } else if (path.equals("/ListContactRequest")) {
      ListContactRequest request = new Gson().fromJson(body, ListContactRequest.class);
      result = processListContactRequest(request);
    } else {
      result = new ContactResponse("Unimplemented request " + path);
    }
    
    String jsonResponse = new Gson().toJson(result);
    System.out.println("Sending response: " + jsonResponse);
    
    return new Response(jsonResponse);
  }
  
  private Object processListContactRequest(ListContactRequest request) {
    ListContactResponse response = new ListContactResponse(
        ContactDatabase.instance().getAll());
    
    return response;
  }

  private Object processAddContactRequest(AddContactRequest request) {
    int newId = ContactDatabase.instance().add(request.contact);
    AddContactResponse response = new AddContactResponse(newId);
    return response;
  }

  protected String getBody(IHTTPSession session) {
    int len = Integer.parseInt(session.getHeaders().get("content-length"));
    InputStream inputStream = session.getInputStream();

    try {
      byte[] buf = new byte[len];
      int bytesRead = 0;
      int read = 0;
      while (bytesRead < len && 
        (read = inputStream.read(buf, bytesRead, len - bytesRead)) > 0) {
          bytesRead += read;        
      }
      return new String(buf);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return "";
  }

  public static void main(String[] args) throws Throwable {
    ContactServer server = new ContactServer(8080);
    try {
      server.start();
    } catch (BindException ioe) {
      System.err.println("Couldn't start server: port is already in use (is another instance running?)");
      System.exit(-1);
    } catch (IOException ioe) {
      System.err.println("Couldn't start server:\n" + ioe);
      System.exit(-1);
    }

    System.out.println("Server started, Hit Enter to stop.\n");

    System.in.read();

    server.stop();
    System.out.println("Server stopped.\n");
  }
  
}
