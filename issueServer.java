import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.util.ArrayList;

import com.google.gson.Gson;


public class issueServer extends NanoHTTPD {
	
	public issueServer(int port) {
		super(port);
	}
	
    @Override
    public synchronized Response serve(IHTTPSession session) {

      String path = session.getUri();
      String body = getBody(session);
      Object result;
      System.out.println("Incoming request: path = " + path + "; body: " + body);
    
      if (path.equals("/AddIssueRequest")) {
        AddIssueRequest request = new Gson().fromJson(body, AddIssueRequest.class);
        result = processAddIssueRequest(request);
      } else if (path.equals("/ListIssueRequest")) {
        ListIssueRequest request = new Gson().fromJson(body, ListIssueRequest.class);
        result = processListIssueRequest(request);
      } else if (path.equals("/ListProjectRequest")) {
    	ListProjectRequest request = new Gson().fromJson(body, ListProjectRequest.class);
    	result = processListProjectRequest(request);
      } else if (path.equals("/EditIssueRequest")) {
    	  EditIssueRequest request = new Gson().fromJson(body, EditIssueRequest.class);
    	  result = processEditIssueRequest(request);
      } else if (path.equals("/DeleteIssueRequest")) {
    	  DeleteIssueRequest request = new Gson().fromJson(body, DeleteIssueRequest.class);
    	  result = processDeleteIssueRequest(request);
      } else {
        result = new IssueResponse("Unimplemented request " + path);
      }
    String jsonResponse = new Gson().toJson(result);
    System.out.println("Sending response: " + jsonResponse);
    
    return new Response(jsonResponse);
  }




	private Object processAddIssueRequest(AddIssueRequest request) {
		int newId = IssueDatabase.instance().add(request.issue);
		AddIssueResponse response = new AddIssueResponse(newId);
		ProjectDatabase.instance().getProject(request.projectId).addIssue(request.issue);
		return response;
	}
	
	
	private Object processEditIssueRequest(EditIssueRequest request) {
		int Id = IssueDatabase.instance().edit(request.issue);
		EditIssueResponse response = new EditIssueResponse(Id);
		ArrayList<Issue> issues = 	ProjectDatabase.instance().getProject(request.projectId).getIssues();
		for(int i = 0; i < issues.size(); i++){
			if (issues.get(i).equals(request.issue)){
				issues.remove(i);
				issues.add(i, request.issue);
			}
				
		}
		/*for (Issue i : issues){
			if (i.getId() == request.issue.getId()){
				issue = i;
			}
		}*/
		return response;
	}

	private Object processListIssueRequest(ListIssueRequest request) {
	    ListIssueResponse response = new ListIssueResponse(
	    		IssueDatabase.instance().getAll());
	    return response;
	}
	
	private Object processListProjectRequest(ListProjectRequest request) {
		ListProjectResponse response = new ListProjectResponse (
				ProjectDatabase.instance().getAll());
		return response;
	}

	private Object processDeleteIssueRequest(DeleteIssueRequest request) {
		int Id = IssueDatabase.instance().delete(request.issue);
		DeleteIssueResponse response = new DeleteIssueResponse(Id);
		Project project = ProjectDatabase.instance().getProject(request.projectId);
		ArrayList<Issue> issues = project.getIssues();
		issues.remove(request.issue);
//	    ProjectDatabase.instance().getProject(request.projectId).getIssues().remove(request.issue);
		return response;
	}
	
	  protected String getBody(IHTTPSession session) {
		    int len = Integer.parseInt(session.getHeaders().get("content-length"));
		    InputStream inputStream = session.getInputStream();

		    try {
		      byte[] buf = new byte[len];
		      int bytesRead = 0;
		      int read = 0;
		      while (bytesRead < len && 
		        (read = inputStream.read(buf, bytesRead, len - bytesRead)) > 0) {
		          bytesRead += read;        
		      }
		      return new String(buf);
		    } catch (IOException ex) {
		      ex.printStackTrace();
		    }
		    return "";
		  }

	  
	  
	  public static void main(String[] args) throws Throwable {
		  
		  startUp();
		 
		    issueServer server = new issueServer(8080);
		    try {
		      server.start();
		    } catch (BindException ioe) {
		      System.err.println("Couldn't start server: port is already in use (is another instance running?)");
		      System.exit(-1);
		    } catch (IOException ioe) {
		      System.err.println("Couldn't start server:\n" + ioe);
		      System.exit(-1);
		    }

		    System.out.println("Server started, Hit Enter to stop.\n");

		    System.in.read();

		    server.stop();
		    System.out.println("Server stopped.\n");
		    
		    shutDown();
		  }
	  
	  public static void startUp()
	  {
		  try
		  {
			  ArrayList<Project> projects = ProjectDatabase.instance().getAll();
			  File f = new File("Projects.dat");
			  if(!f.exists()){
				  projects.add(new Project(0,"Half Life 3"));
				  projects.add(new Project(1,"Half Life 4"));
				  projects.add(new Project(2,"World Domination"));
				  
				  return;
			  }
			  
			  FileInputStream fis = new FileInputStream("Projects.dat");
			  ObjectInputStream ois = new ObjectInputStream(fis);
			  try{ 
				  while(projects.add((Project)ois.readObject()));
			  } catch (EOFException eof){
				  ois.close();
			  }
			  
			  
		  }
		  catch(Exception e) {
			  System.out.println("Error In startup: " + e);
			  e.printStackTrace();
		  }
	  }
	  
	  public static void shutDown()
	  {
		  try
		  {
			  FileOutputStream fos = new FileOutputStream("Projects.dat");
			  ObjectOutputStream oos = new ObjectOutputStream(fos);
			  ArrayList<Project> projects = ProjectDatabase.instance().getAll();
			  for(Project p : projects)
			  {
				  oos.writeObject(p);
			  }
			  oos.close();
			  ProjectDatabase.instance().clearAll();
		  }
		  catch(Exception e) {System.out.println("Error In shutdown: " + e);}
	  }
}
	
	
	
