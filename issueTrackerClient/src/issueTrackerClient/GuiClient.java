//package issueTrackerClient;
//
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.google.gson.Gson;

public class GuiClient implements ActionListener, TableModelListener, ListSelectionListener{
	
	private static JFrame w;
	
	private static JButton create;
	private static JButton del;
	
	private static JList list;
	private static JTable table;
	
	private static ArrayList<Project> projects;
	private static Vector<Vector<Object>> issues;
	
	private static GuiClient instance = new GuiClient();
	
	public static void main(String args[]){
		
		w = new JFrame("Issue Tracker Client");
		w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		create = new JButton("Create");
		del = new JButton("Delete");
		
		create.addActionListener(instance);
		del.addActionListener(instance);
		

		
		projects = getProjects();
		issues = new Vector<Vector<Object>>();
		
		
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("Id");
		columnNames.add("Title");
		columnNames.add("Description");
		columnNames.add("Status");
		
		table = new JTable(issues, columnNames){
			public boolean isCellEditable(int r, int c){
				return c!=0;
			}
		};
		table.getModel().addTableModelListener(instance);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getColumn(table.getColumnName(3)).setCellEditor(new DefaultCellEditor(new JComboBox(Status.values())));
		JScrollPane sp = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		String projectNames[] = new String[projects.size()];
		for (int i=0; i<projects.size(); i++)
			projectNames[i] = projects.get(i).getTitle();
		
		list = new JList(projectNames);
		for (Project p: projects){
			list.add(new JLabel(p.getTitle()));
		}
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(instance);
		list.setSelectedIndex(0);
		//for (Issue i: projects.get(list.getSelectedIndex()).getIssues()){
		//	issues.add(IssuetoVector(i));
		//}
		
		Container c = w.getContentPane();
		c.setLayout(new BorderLayout());
		
		JPanel outer = new JPanel();
		BorderLayout bl = new BorderLayout();
		bl.setHgap(10);
		outer.setLayout(bl);
		outer.add(list, BorderLayout.WEST);
		outer.add(sp, BorderLayout.CENTER);
		
		JPanel south = new JPanel();
		south.setLayout(new GridLayout(1,2));
		south.add(create);
		south.add(del);
		
		c.add(outer, BorderLayout.CENTER);
		c.add(south, BorderLayout.SOUTH);

		w.setSize(500, 500);
		w.setVisible(true);
	}
	
	public static ArrayList<Project> getProjects(){
		String jsonResponse = sendProjectRequest(new ListProjectRequest());
        
        ListProjectResponse response = new Gson().fromJson(jsonResponse, ListProjectResponse.class);
        return response.projects;
	}
	
	public void actionPerformed(ActionEvent e){
		if (e.getSource() == create){
			Issue issue = new Issue(0, "", "", Status.NEW);
			issue.setId(getNewIssue(projects.get(list.getSelectedIndex()).getId(), issue));
			
			issues.add(IssuetoVector(issue));
			projects.get(list.getSelectedIndex()).getIssues().add(issue);
			table.updateUI();
		} else if (e.getSource() == del){
			if (table.getSelectedRow() == -1 || table.getSelectedRow() >= table.getRowCount())
				return;
			Issue issue = IssuefromVector(issues.remove(table.getSelectedRow()));
			projects.get(list.getSelectedIndex()).getIssues().remove(issue);
			issueDelete(projects.get(list.getSelectedIndex()).getId(), issue);
			table.updateUI();
		}
	}

	@Override
	public void tableChanged(TableModelEvent e){
		Issue issue = IssuefromVector(issues.elementAt(table.getSelectedRow()));
		for (Issue i: projects.get(list.getSelectedIndex()).getIssues()){
			if (i.getId() == issue.getId()){
				i.setTitle(issue.getTitle());
				i.setDesc(issue.getDesc());
				i.setStatus(issue.getStatus());
			}
		}
		
		issueEdit(projects.get(list.getSelectedIndex()).getId(), issue);
		table.updateUI();
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting())
			return;
		issues.removeAllElements();
		table.updateUI();

		for (Issue i: projects.get(list.getSelectedIndex()).getIssues()){
			issues.add(IssuetoVector(i));
		}
	}
	
	private static Vector<Object> IssuetoVector(Issue i){
		Vector<Object> v = new Vector<Object>();
		v.add(i.getId());
		v.add(i.getTitle());
		v.add(i.getDesc());
		v.add(i.getStatus());
		return v;
	}
	
	private static Issue IssuefromVector(Vector<Object> v){
		return new Issue((Integer)(v.elementAt(0)), (String)(v.elementAt(1)), (String)(v.elementAt(2)), (Status)(v.elementAt(3)));
	}
	
	static String sendRequest(IssueRequest req) {
        String jsonRequest = new Gson().toJson(req);
        String url = "http://localhost:8080/" + req.getClass().getSimpleName();
        System.out.println("Sending to " + url + " - " + jsonRequest);
       
        HttpRequest httpReq = HttpRequest.post(url).send(jsonRequest);
        int status = httpReq.code();
        String jsonResponse = httpReq.body();
       
        System.out.println("Received code " + status + " json " + jsonResponse);
        return jsonResponse;
           
      }
	
	static String sendProjectRequest(ProjectRequest req) {
        String jsonRequest = new Gson().toJson(req);
        String url = "http://localhost:8080/" + req.getClass().getSimpleName();
        System.out.println("Sending to " + url + " - " + jsonRequest);
       
        HttpRequest httpReq = HttpRequest.post(url).send(jsonRequest);
        int status = httpReq.code();
        String jsonResponse = httpReq.body();
       
        System.out.println("Received code " + status + " json " + jsonResponse);
        return jsonResponse;
           
      }
	
	static int getNewIssue(int id, Issue issue){
		String jsonResponse = sendRequest(new AddIssueRequest(issue,id ));
        
        AddIssueResponse response = new Gson().fromJson(jsonResponse, AddIssueResponse.class);
        return response.getNewId();
	}
	
	static void issueDelete(int id, Issue issue){
		String jsonResponse = sendRequest(new DeleteIssueRequest(issue, id));
        
        DeleteIssueResponse deleteResponse = new Gson().fromJson(jsonResponse, DeleteIssueResponse.class);
	}
	
	static void issueEdit(int id, Issue issue){
		String jsonResponse = sendRequest(new EditIssueRequest(issue,id));
        
        EditIssueResponse editResponse = new Gson().fromJson(jsonResponse, EditIssueResponse.class);
	}
}