package issueTrackerClient;

import java.util.ArrayList;

public class Project{
	private String Title;
	private ArrayList<Issue> Issues;
	private int id;
	
	public Project(int id, String title){
			this.id = id;
			this.Title = title;
			this.Issues = new ArrayList<Issue>();
			}

	public String getTitle() {
		return Title;
	}

	public ArrayList<Issue> getIssues() {
		return Issues;
	}
			
	public int getId(){
		return id;
	}
	
	public void setId(int a){
		this.id = a;
	}
	
	public String toString(){
		return this.Title;
	}
}
